<?php


namespace Hylm\LaravelSwoole\Swoole\Facades;


use Illuminate\Support\Facades\Facade;

class SwooleFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'swoole.server';
    }
}