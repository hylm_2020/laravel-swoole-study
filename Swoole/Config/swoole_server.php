<?php
return [
    /*
   |--------------------------------------------------------------------------
   | HTTP服务器配置。
   |--------------------------------------------------------------------------
   */
    'server' => [
        'host' => env('SWOOLE_HTTP_HOST', '0.0.0.0'),
        'port' => env('SWOOLE_HTTP_PORT', 9501),
        'public_path' => base_path('public'),
        //确定是否使用swoole响应对静态文件的请求
        'handle_static_files' => env('SWOOLE_HANDLE_STATIC', true),
        'access_log' => env('SWOOLE_HTTP_ACCESS_LOG', false),
        //使用SSL必须在编译swoole时加入--enable-openssl选项
        //如果要启用SSL，请输入“SWOOLE_SOCK_TCP | SWOOLE_SSL
        'socket_type' => SWOOLE_SOCK_TCP,
        //$mode运行的模式: SWOOLE_BASE基本模式 | SWOOLE_PROCESS 多进程模式（默认）
        'process_type' => SWOOLE_PROCESS,
        //设置Server运行时的各项参数
        'options' => [
            'pid_file' => env('SWOOLE_HTTP_PID_FILE', base_path('storage/logs/swoole_http.pid')),
            'log_file' => env('SWOOLE_HTTP_LOG_FILE', base_path('storage/logs/swoole_http_' . date('Y-m-d') . '.log')),
            //是否开启守护进程，不启用守护进程，当ssh终端退出后，程序将被终止运行, 使用systemd管理Swoole服务时，请勿设置daemonize = 1
            'daemonize' => env('SWOOLE_HTTP_DAEMONIZE', true),
            //通常，根据您的cpu核数，这个值应该是1~4倍。
            'reactor_num' => env('SWOOLE_REACTOR_NUM', swoole_cpu_num()),
            'worker_num' => env('SWOOLE_HTTP_WORKER_NUM', swoole_cpu_num()),
            'task_worker_num' => env('SWOOLE_HTTP_TASK_WORKER_NUM', swoole_cpu_num()),
            //检测死连接:1-开启|0-关闭
            'open_tcp_keepalive' => 1,
            //设置最大数据包尺寸，单位为字节
            'package_max_length' => 20 * 1024 * 1024,
            //配置发送输出缓存区内存尺寸
            'buffer_output_size' => 10 * 1024 * 1024,
            //
            'socket_buffer_size' => 128 * 1024 * 1024,
            //设置worker进程的最大任务数
            'max_request' => 3000,
            // 使协同程序发送
            'send_yield' => false,
            //cert证书和key私钥的路径, 使用SSL必须在编译swoole时加入--enable-openssl选项
            'ssl_cert_file' => null,
            'ssl_key_file' => null,
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | 启用websocket服务器。HTTP服务器配置。
    |--------------------------------------------------------------------------
    */
    'websocket' => [
        //是否开启websocket：true-开启|false-关闭
        'enabled' => env('SWOOLE_HTTP_WEBSOCKET', false),
    ],

    /*
    |--------------------------------------------------------------------------
    | 热重新加载配置
    |--------------------------------------------------------------------------
    */
    'hot_reload' => [
        'enabled'     => env('SWOOLE_HOT_RELOAD_ENABLE', false),
        'recursively' => env('SWOOLE_HOT_RELOAD_RECURSIVELY', true),
        'directory'   => env('SWOOLE_HOT_RELOAD_DIRECTORY', base_path()),
        'log'         => env('SWOOLE_HOT_RELOAD_LOG', true),
        'filter'      => env('SWOOLE_HOT_RELOAD_FILTER', '.php'),
    ],

    /*
    |--------------------------------------------------------------------------
    |如果启用，控制台输出将被转移到响应内容。
    |--------------------------------------------------------------------------
    */
    'ob_output' => env('SWOOLE_OB_OUTPUT', true),
];
