<?php

namespace Hylm\LaravelSwoole\Swoole;

use Hylm\LaravelSwoole\Swoole\Console\HttpServerCommand;
use Hylm\LaravelSwoole\Swoole\Manager\PidManager;
use Hylm\LaravelSwoole\Swoole\Manager\SwooleManager;
use Illuminate\Support\ServiceProvider;
use Swoole\Http\Server as SwooleHttpServer;
use Swoole\WebSocket\Server as SwooleWebSocket;

class SwooleServiceProvider extends ServiceProvider
{
    protected $command = [
        HttpServerCommand::class
    ];

    protected static $server;

    //服务提供器注册
    public function register()
    {
        $this->registerConfig();
        $this->registerSwooleServer();
        $this->registerSwooleManager();
        $this->commands($this->command);
    }

    public function boot()
    {

    }

    //注册配置文件
    public function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/Config/swoole_server.php', 'swoole');
    }

    /**
     * 注册swoole服务绑定到服务容器中
     *
     * 如果你的服务提供器注册了许多简单的绑定，可用 bindings 和 singletons 属性替代手动注册每个容器绑定。
     * 当服务提供器被框架加载时，将自动检查这些属性并注册相应的绑定
     */
    protected function registerSwooleManager()
    {
        $this->app->singleton('swoole.manager', function ($app) {
            return new SwooleManager($app);
        });
    }

    //注册pid管理
    protected function registerPidManager(): void
    {
        $this->app->singleton(PidManager::class, function () {
            return new PidManager(
                $this->app->make('config')->get('swoole.server.options.pid_file')
            );
        });
    }

    //注册swoole的服务
    protected function registerSwooleServer()
    {
        // 把swoole注册到laravel的ioc（服务容器）
        $this->app->singleton('swoole.server', function () {
            if (is_null(self::$server)) {
                $this->createSwooleServer();
                $this->configureSwooleServer();
            }
            return static::$server;
        });
    }

    /**
     * 创建swoole服务
     * @return \Swoole\Http\Server | \Swoole\WebSocket\Server
     */
    private function createSwooleServer()
    {
        // 根据配置文件, 确定要创建服务类型
        $server = config('swoole.websocket.enabled') ? SwooleWebSocket::class : SwooleHttpServer::class;
        $host = config('swoole.server.host');
        $port = config('swoole.server.port');
        $socketType = config('swoole.server.socket_type', SWOOLE_SOCK_TCP);
        $processType = config('swoole.server.process_type', SWOOLE_PROCESS);

        static::$server = new $server($host, $port, $processType, $socketType);

    }

    /**
     * 设置swoole服务器配置。
     */
    private function configureSwooleServer()
    {
        $options = config('swoole.server.options');
        // 仅在websocket模式和队列驱动中启用 task_worker
        if (!config('swoole.websocket.enabled')) {
            unset($options['task_worker_num']);
        }

        static::$server->set($options);
    }
}
