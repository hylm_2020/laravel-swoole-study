<?php

namespace Hylm\LaravelSwoole\Swoole\Console;

use Hylm\LaravelSwoole\Swoole\Manager\PidManager;
use Hylm\LaravelSwoole\Swoole\Helpers\OS;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Swoole\Process;


class HttpServerCommand extends Command
{
    /**
     * 控制台命令的名称和签名。
     *
     * @var string
     */
    protected $signature = 'swoole:http {action : start|stop|restart|reload|infos}';

    /**
     * 控制台命令的描述。
     *
     * @var string
     */
    protected $description = 'Swoole HTTP服务器控制器。';

    protected $manager;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->checkEnvironment();
        $this->execution();
    }

    // 执行
    protected function execution()
    {
        $action = $this->argument('action');

        if (!in_array($action, ['start', 'stop', 'restart', 'reload', 'infos'], true)) {
            $this->error(
                "非法参数 '{$this->action}'. 必须是 'start', 'stop', 'restart', 'reload' or 'infos'的值."
            );
        }

        $this->{$action}();
    }

    // 运行swoole_http_server服务
    protected function start()
    {
        if ($this->isRunning()) {
            $this->error("运行失败! swoole_http_server 进程已经在运行.");
            return;
        }
        $this->manager = $this->laravel->make('swoole.manager');
        $this->info("Swoole服务成功运行。");
        $this->manager->runAction();
    }

    // 停止swoole_http_server服务
    protected function stop()
    {
        if (!$this->isRunning()) {
            $this->error("运行失败! 没有正在运行的swoole_http_server进程.");
            return;
        }

        $this->info('停止swoole_http_server...');

        if ($this->killProcess()) {
            $this->error('无法停止swoole_http_server进程');
            return;
        }

        $this->laravel->make(PidManager::class)->delete();
        $this->info('> succes');
    }

    // 重启swoole服务
    protected function restart()
    {
        if ($this->isRunning()) {
            $this->stop();
        }
        $this->start();
    }

    // 重载swoole服务
    protected function reload()
    {
        if (!$this->isRunning()) {
            $this->error('失败了!没有正在运行的swoole_http_server进程');
            return;
        }

        $this->info('重载swoole_http_server...');

        if (!$this->killProcess()) {
            $this->error('> failure');
            return;
        }

        $this->info('> success');
    }

    // 显示PHP和Swoole杂项信息
    protected function infos()
    {
        $this->showInfos();
    }

    // 显示PHP和Swoole的杂项信息。
    protected function showInfos()
    {
        $isRunning = $this->isRunning();
        $host = config('swoole.server.host');
        $port = config('swoole.server.port');
        $pids = $this->laravel->make(PidManager::class)->read();
        $masterPid = $pids['masterPid'] ?? null;
        $managerPid = $pids['managerPid'] ?? null;
        $reactorNum = config('swoole.server.options.reactor_num');
        $workerNum = config('swoole.server.options.worker_num');
        $taskWorkerNum = config('swoole.server.options.task_worker_num');
        $isWebsocket = config('swoole.websocket.enabled');
        $pidFile = config('swoole.server.options.pid_file');
        $logFile = config('swoole.server.options.log_file');
        $table = [
            ['PHP Version', 'Version' => phpversion()],
            ['Swoole Version', 'Version' => swoole_version()],
            ['Laravel Version', $this->getApplication()->getVersion()],
            ['Listen IP', $host],
            ['Listen Port', $port],
            ['Server Status', $isRunning ? 'Online' : 'Offline'],
            ['Reactor Num', $reactorNum],
            ['Worker Num', $workerNum],
            ['Task Worker Num', $taskWorkerNum],
            ['Websocket Mode', $isWebsocket ? 'On' : 'Off'],
            ['Master PID', $isRunning ? $masterPid : 'None'],
            ['Manager PID', $isRunning && $managerPid ? $managerPid : 'None'],
            ['Pid Path', $pidFile],
            ['Log Path', $logFile],
        ];

        $this->table(['Name', 'Value'], $table);
    }

    /**
     * 检查运行环境。
     */
    protected function checkEnvironment()
    {

        if (OS::is(OS::WIN)) {
            $this->error('Swoole extension doesn\'t support Windows OS.');

            exit(1);
        }

        if (!extension_loaded('swoole')) {
            $this->error('Can\'t detect Swoole extension installed.');

            exit(1);
        }

        if (!version_compare(swoole_version(), '4.3.1', 'ge')) {
            $this->error('Your Swoole version must be higher than `4.3.1`.');

            exit(1);
        }
    }

    /**
     * 检查Swoole进程是否正在运行
     *
     * @return bool 是-true|否-false
     * Process::kill($pid, $signo = SIGTERM); 向指定pid进程发送信号
     * 默认的信号为SIGTERM，表示终止进程
     * $signo=0，可以检测进程是否存在，不会发送信号
     */
    protected function isRunning()
    {
        $pids = $this->laravel->make(PidManager::class)->read();

        $masterPid = $pids['masterPid'] ?? null;
        $managerPid = $pids['managerPid'] ?? null;
        // 在 Swoole\Server 中不能设置某些信号监听，如 SIGTERM 和 SIGALAM
        if ($managerPid) {
            return $masterPid && $managerPid && Process::kill((int)$managerPid, 0);
        }
        return $masterPid && Process::kill((int)$masterPid, 0);
    }

    // 杀死进程
    protected function killProcess()
    {
        $pids = $this->laravel->make(PidManager::class)->read();

        foreach ($pids as $pid) {
            exec("sudo kill -9 $pid", $output, $return_var);
        }

        return $this->isRunning();
    }
}
