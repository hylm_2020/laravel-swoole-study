<?php

namespace Hylm\LaravelSwoole\Swoole\Manager;

use Hylm\LaravelSwoole\Swoole\Helpers\OS;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Http\Kernel;
use Hylm\LaravelSwoole\Swoole\Http\SRequest;
use Hylm\LaravelSwoole\Swoole\Http\SResponse;


class SwooleManager
{
    /**
     * 保存swoole服务
     * @var \Swoole\Http\Server | \Swoole\WebSocket\Server
     */
    protected $server;

    /**
     * laravel的应用程序Application
     * @var [type]
     */
    protected $laravel;

    /**
     * swoole监听事件
     * 写法：swoole监听事件方法 => 当前类中对应的方法 例：request => onRequest
     * @var array
     */
    protected $events = [
        'http' => [
            'start' => 'onStart',
            'request' => 'onRequest',
//            'workerStart' => 'onWorkerStart',
//            'workerStop'  => 'onWorkStop',
        ],
        'websocket' => [
            'open' => 'onOpen',
            'message' => 'onMessage',
            'close' => 'onClose'
        ],
    ];

    public function __construct(Container $container)
    {
        $this->laravel = $container;
        // ... 获取swoole的服务
        $this->server = $this->laravel->make('swoole.server');
        // ... 设置swoole的监听函数
        $this->setSwooleServerEvent();
    }

    /**
     * 设置swoole的监听函数
     */
    protected function setSwooleServerEvent()
    {
        $type = config('swoole.websocket.enabled') ? 'websocket' : 'http';
        foreach ($this->events[$type] as $event => $func) {
            $this->server->on($event, [$this, $func]);
        }
    }

    public function onStart($server)
    {
        $this->setProcessName('master process');
        $this->laravel->make(PidManager::class)->write($server->master_pid, $server->manager_pid ?? 0);
    }

    // "onRequest"监听器
    public function onRequest($swooleRequest, $swooleResponse)
    {
        try {

            $laravelRequest = SRequest::make($swooleRequest);

            $laravelResponse = $this->laravel->make(Kernel::class)->handle($laravelRequest);
            //设置响应头，防止跨域
            $swooleResponse->header("Access-Control-Allow-Origin", "*");
            $swooleResponse->header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
            $swooleResponse->header("Access-Control-Allow-Credentials", true);
            $swooleResponse->header("Access-Control-Expose-Headers", "Authorization, authenticated");
            // 页面渲染
            SResponse::make($laravelResponse, $swooleResponse)->send();

        } catch (\Exception $e) {
            $swooleResponse->end($e);
        }

    }

    /**
     * "onWorkerStart" 监听器.
     *
     * @param \Swoole\Http\Server|mixed $server
     *
     * @throws \Exception
     */
    public function onWorkerStart($server)
    {
        $this->clearCahe();
        $this->laravel->make('events')->dispatch('swoole.workerStart', func_get_args());
        // 设置进程名称。
        $this->setProcessName($server->taskworker ? 'task process' : 'worker process');
        // 清除事件实例，以防在工作进程中重复监听器
        Facade::clearResolvedInstance('events');

        // 准备laravel应用
        $this->laravel->getApplication();

        // 设置laravel app后绑定
        $this->bindToLaravelApp();

        // 准备websocket处理程序和路由
        if (config('swoole.websocket.enabled')) {
//            $this->prepareWebsocketHandler();
//            $this->loadWebsocketRoutes();
        }
    }

    public function runAction()
    {
        $this->server->start();
    }

    /**
     * 设置进程名称。
     *
     * @codeCoverageIgnore
     *
     * @param $process
     */
    protected function setProcessName($process)
    {
        // MacOS不支持修改进程名
        if (OS::is(OS::MAC_OS, OS::CYGWIN) || $this->isInTesting()) {
            return;
        }
        $serverName = 'swoole_http_server';
        $appName = $this->laravel->make('config')->get('app.name', 'Laravel');

        $name = sprintf('%s: %s for %s', $serverName, $process, $appName);

        swoole_set_process_name($name);
    }

    // 清除APC或OPCache。
    protected function clearCache()
    {
        if (extension_loaded('apc')) {
            apc_clear_cache();
        }

        if (extension_loaded('Zend OPcache')) {
            opcache_reset();
        }
    }

    /**
     * 指示它是否在phpunit环境中
     *
     * @return bool
     */
    protected function isInTesting()
    {
        return defined('IN_PHPUNIT') && IN_PHPUNIT;
    }
}
