<?php


namespace Hylm\LaravelSwoole\Swoole\Manager;


class PidManager
{
    protected $pidFile;

    public function __construct(string $pidFile = null)
    {
        $this->setPidFile($pidFile ?: sys_get_temp_dir() . '/swoole.pid');
    }

    // 设置pid文件路径
    public function setPidFile(string $pidFile): self
    {
        $this->pidFile = $pidFile;

        return $this;
    }

    /**
     * 将主pid和管理pid写入pid文件
     *
     * @throws \RuntimeException 当$pidFile是不可写的
     */
    public function write(int $masterPid, int $managerPid): void
    {
        if (!is_writable($this->pidFile)
            && !is_writable(dirname($this->pidFile))
        ) {
            throw new \RuntimeException(sprintf('Pid file "%s" is not writable', $this->pidFile));
        }

        file_put_contents($this->pidFile, $masterPid . ',' . $managerPid);
    }

    /**
     * 从pid文件中读取主pid和管理pid
     *
     * @return string[] {
     * @var string $masterPid
     * @var string $managerPid
     * }
     */
    public function read(): array
    {
        $pids = [];
        if (is_readable($this->pidFile)) {
            $content = file_get_contents($this->pidFile);
            $pids = explode(',', $content);
        }
        return [
            'masterPid' => $pids[0] ?? null,
            'managerPid' => $pids[1] ?? null
        ];
    }

    /**
     * 获取pid文件路径
     *
     * @return string
     */
    public function file()
    {
        return $this->pidFile;
    }

    /**
     * 删除pid文件
     */
    public function delete(): bool
    {
        if (is_writeable($this->pidFile)){
            return unlink($this->pidFile);
        }
        
        return false;
    }
}
