<?php


namespace Hylm\LaravelSwoole\Swoole\Helpers;


use Illuminate\Support\Str;

class OS
{
    /**
     * Mac OS系统
     *
     * @const string
     */
    public const MAC_OS = 'dar';

    /**
     * Linux系统
     *
     * @const string
     */
    public const LINUX = 'lin';

    /**
     * Windows系统
     *
     * @const string
     */
    public const WIN = 'win';

    /**
     * Cygwin
     *
     * @const string
     */
    public const CYGWIN = 'cyg';

    public static function is(string ...$type): bool
    {
        return Str::contains(static::current(), $type);
    }

    public static function current(): string
    {
        return Str::substr(Str::lower(PHP_OS), 0, 3);
    }

}
