<?php

namespace Hylm\LaravelSwoole\Swoole\Http;

use Illuminate\Http\Response as LaravelResponse;
use Swoole\Http\Response as SwooleResponse;

class SResponse
{
    protected $laravelResponse;
    protected $swooleResponse;

    public function __construct($laravelResponse, $swooleResponse)
    {
        $this->laravelResponse = $laravelResponse;
        $this->swooleResponse = $swooleResponse;
    }

    // 解析swoole的请求
    public static function make($laravelResponse, $swooleResponse)
    {
        return new static($laravelResponse, $swooleResponse);
    }

    public function send()
    {
        $this->sendHandler();
        $this->sendContent();
    }

    public function sendHandler()
    {
        $laravelResponse = $this->laravelResponse;
        //1.设置请求头
        $headers = $laravelResponse->headers->allPreserveCase();
    
        //因为swoole有自己的设置token的方式所以这里需要删除
        if (isset($headers['Set-Cookie'])) {
            unset($headers['Set-Cookie']);
        }
        foreach ($headers as $name => $values) {
            foreach ($values as $value) {
                $this->swooleResponse->header($name, $value);
            }
        }
//        $this->swooleResponse->header("Access-Control-Allow-Origin", "*");
        // 2. 设置状态
        $this->swooleResponse->status($laravelResponse->getStatusCode());
        // 3. 设置cookie
        // $illuminateResponse->headers : Symfony\Component\HttpFoundation\ResponseHeaderBag
        foreach ($laravelResponse->headers->getCookies() as $cookie) {
            // $cookie : Symfony\Component\HttpFoundation\Cookie;
            $this->swooleResponse->cookie(
                $cookie->getName(),
                $cookie->getValue(),
                $cookie->getExpiresTime(),
                $cookie->getPath(),
                $cookie->getDomain(),
                $cookie->isSecure(),
                $cookie->isHttpOnly()
            );
        }
    }

    // 输出主要内容
    public function sendContent()
    {
        $this->swooleResponse->end($this->laravelResponse->getContent());
    }
}
